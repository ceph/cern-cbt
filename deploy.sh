#!/bin/bash
# a orchestration script for deploying, and reseting different elements needed to run CBT on a CERN ceph cluster, with CERN openstack clients.
CLUSTER="clare"

usage() {
 echo "a orchestration script for deploying, and reseting different elements needed to run CBT on a CERN ceph cluster, with CERN openstack clients."
 echo "Usage: $0 [OPTIONS]"
 echo "Options:"
 echo " -k  --reset-known	reset known host list and print client yaml for cbt config, run after changing the clients or ceph nodes partaking in cbt"
 echo " -d  --retar-dependency	retars the files in ./dependency for copying. run after making a change to dependencies that you wish to propagate with -c or -s" 
 echo " -c  --reset-client   	copy dependencies to clients, run after changing a dependency file that affects clients"
 echo " -s  --reset-server   	copy dependencies to cluster nodes, run after changing a dependency file that affects ceph servers"
 echo " -C  --deploy-client  	deploy client dependencies using ./client/client-setup.sh locally, after deploying with -c"
 echo " -S  --deploy-server  	deploy ceph cluster dependencies using ./server/server-setup.sh locally after deploying with -s"
}

if [[ $# = 0 ]]; then
	usage
	exit
fi

# only ever generate node lists once, and pass as arguments from here otherwise we 
# risk some sort of inconsistancy between subshell scripts and the nodes worked on
eval $(ai-rc "IT Ceph Ironic" --region "pdc")
CLUSTER_NODES=$(ai-foreman -l ceph/"$CLUSTER" showhost | awk '{print $2}' | grep ceph | xargs) 
eval $(ai-rc "Ceph NVMe Scale Testing" --region "pdc")

# Client node list is too big to pass as an argument list directly to most of the commands in this script
# for whatever reason, bash arrays do not incur a stack memory size limit, and therefore wont elicit a "ARGUMENTS_TOO_LONG_ERROR"
# however expandign the array directly, here can cause problems with argument parsing 
# therefore we split sections using the array off, and pass as an arg allowing us to exceed the stack limit, without seeing expansion wierdness
mapfile -t CLIENT_NODES < <(openstack server list | awk '{print $4}' | grep pdc | xargs)

# handle argument flags, check usage func above for synopsis
while [[ $# -gt 0 ]]; do
	case "$1" in
		-k|--reset-known)
			# setup client yaml
			openstack server list -f 'json' > ./client/temp.json # json is easier to parse to a yaml array
			./client/gen-client-yaml.sh
			rm ./client/temp.json
			# setup known hosts
			./gen-known-hosts.sh "$CLUSTER_NODES" "${CLIENT_NODES[@]}" # too big directly, fork.  
		;;
		-d|--retar-dependency)
			rm ./dependency/dependency.tar
			tar -ca ./dependency/* -f ./dependency/dependency.tar
			echo "re-tar of dependency's complete, make sure to deploy using -c || -s"
		;;
		
		-c|--reset-client)
			echo "${CLIENT_NODES[@]}" | xargs -d ' ' -P 200 -I% rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=./dependency/known_hosts" -auP  ./dependency/dependency.tar root@%:/tmp/
			echo "${CLIENT_NODES[@]}" | xargs -d ' ' -P 200 -I% rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=./dependency/known_hosts" -auP  ./client/client-setup.sh root@%:/root/
			echo "client reset complete, deploy with -C"
		;;	
		
		-s|--reset-server)
			echo "$CLUSTER_NODES" | xargs -d ' ' -P 200 -I% rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=./dependency/known_hosts" -auP  ./dependency/dependency.tar root@%:/tmp/
			echo "$CLUSTER_NODES" | xargs -d ' ' -P 200 -I% rsync -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=./dependency/known_hosts" -auP  ./server/server-setup.sh root@%:/root/
			echo "server reset complete, deploy with -S"
		;;
	
		-C|--deploy-client)
			./client/deploy-client.sh "${CLIENT_NODES[@]}" # too big directly, fork. 
		;;
		
		-S|--deploy-server)
			echo "warning, this flag can DAMAGE a ceph cluster, ctrl+c now if usage is unclear."
			sleep 6
			echo "starting"
			wassh --verbose --ssh-opts '-o UserKnownHostsFile=./dependency/known_hosts' -l root -p 50 -h "$CLUSTER_NODES" 'bash /root/server-setup.sh'
			echo "server deploy complete"
	    	;;
		
		*)
	      		echo "Unknown option: $1"
	      		exit 1
	      	;;
	esac
	shift
done

echo 'fin.'
exit # xargs can lock up process termination when ran in parrallel, exit explicitly
