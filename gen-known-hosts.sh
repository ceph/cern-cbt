#!/bin/bash
# Script to generate a known_hosts file for nodes cbt will run on or against

ulimit -s 9999

echo "creating new known_hosts file in $(pwd)/dependency/known_hosts"
rm ./dependency/known_hosts
touch ./dependency/known_hosts

echo "adding cluster hosts"
echo ""
sleep 3
# HOSTNAME
echo $1 | xargs -d ' ' -P 200 -I % sh -c \
"ssh root@% -n -T -o StrictHostKeyChecking=accept-new \
-o UserKnownHostsFile=./dependency/known_hosts 'echo  %: done' || true"

echo "adding client hosts"
echo ""
sleep 3
# FQDN
echo $2 | xargs -d ' ' -P 200 -I % sh -c \
"ssh root@%.cern.ch -n -T -o StrictHostKeyChecking=accept-new \
-o UserKnownHostsFile=./dependency/known_hosts 'echo  %.cern.ch: done' || true"
# HOSTNAME
echo $2 | xargs -d ' ' -P 200 -I % sh -c \
"ssh root@% -n -T -o StrictHostKeyChecking=accept-new \
-o UserKnownHostsFile=./dependency/known_hosts 'echo  %: done' || true"
