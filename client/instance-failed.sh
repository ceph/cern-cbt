#!/bin/bash
# a simple script for deploying a rapid number of clients for cbt workloads
export OS_REGION_NAME=pdc
eval $(ai-rc "Ceph NVMe Scale Testing")

for i in $(cat failed); do
	openstack server create $i --image 1fcfa1d3-51b2-4d49-a4db-5293ebff9a5d --flavor m4.medium --key-name pdc-scale-testing  
done
