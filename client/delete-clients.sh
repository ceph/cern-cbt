#!/bin/bash
# a simple script for deploying a rapid number of clients for cbt workloads
export OS_REGION_NAME=pdc
eval $(ai-rc "Ceph NVMe Scale Testing")

if [ -z "$1" -o -z "$2" ] ; then
	echo 'supply two numbers as a range for the total number of clients to attempt to delete(this range will also form the hostname suffix)'
	exit
fi 

echo 'this script deletes numerous openstack clients, only use if you are certain of the impact' 
echo "script will attempt to delete $(($2 - $1)) hosts in 10 seconds"
sleep 10

for i in $(seq $1 $2); do
	openstack server delete pdc-scale-test-client-$i 
done
