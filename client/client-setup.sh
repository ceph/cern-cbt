#!/bin/bash
# A script for setting up cbt clients elements that have to be run ON each node should go in here.

#setup client dependencies
tar -xvf /tmp/dependency.tar -C /tmp --strip-components=2
mv /tmp/config  /root/.ssh/
mv /tmp/profile /etc/
mv /tmp/known_hosts /root/.ssh/
mv /tmp/pdc-scale-testing /root/
mv /tmp/pdc-scale-testing.pub /root/
mkdir /etc/ceph/
mv /tmp/keyring /etc/ceph
mv /tmp/ceph_client.conf /etc/ceph/ceph.conf
mv /tmp/client_rw.secret /etc/ceph/
mv /tmp/ceph-reef9el-qa.repo /etc/yum.repos.d/

# Setup web proxy and ssh
eval "$(ssh-agent -s)"
ssh-add /root/pdc-scale-testing
git config --global http.proxy http://cephtelemetry-proxy.cern.ch:3128 
source /etc/profile 						      

# package dependencies
dnf update -y
dnf install -y psmisc util-linux coreutils xfsprogs e2fsprogs findutils \
  git wget bzip2 make automake gcc gcc-c++ kernel-devel perf blktrace lsof \
  sysstat screen python3-yaml ipmitool dstat zlib-devel fio iftop iperf3 \
  pdsh pdsh-rcmd-ssh chkconfig ceph ceph-common

# setup collectl
if [ ! -e "/usr/bin/collectl" ]; then
	mv /etc/init.d /tmp/init.d-old # one time, should fail on subsequent runs
	tar -hxvf /tmp/collectl-4.3.8.src.tar.gz -C /root/
	cd /root/collectl/
	./INSTALL
	systemctl daemon-reload
	systemctl start collectl        # start data collection service on host
	systemctl enable collectl       # optional: enable collectl server to be started at boot time
	ls -ltr /var/log/collectl/*     # where output from collectl is kept
fi

# configure other system settings
sed -i 's/Defaults    requiretty/#Defaults    requiretty/g' /etc/sudoers
setenforce 0
( awk '!/SELINUX=/' /etc/selinux/config ; echo "SELINUX=disabled" ) > /tmp/x
mv /tmp/x /etc/selinux/config
systemctl stop nftables && systemctl disable nftables
systemctl stop irqbalance && systemctl disable irqbalance
systemctl restart sshd

# mount cephfs
mkdir -p /cephfs
umount /cephfs # ensure we don't double mount
mount -t ceph cephclare.cern.ch:/ /cephfs -oname=client_rw,secretfile=/etc/ceph/client_rw.secret -o noatime
