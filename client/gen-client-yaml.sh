#!/bin/bash
# A small script to generate a yaml format array of all clients cbt will target in yaml format

jq .[].Name ./client/temp.json  | tr -d '"' | awk '{print "\"" $0 ".cern.ch\","}' |  tr -d '\n' | awk '{print "[" $0 "]"}' > ./client/clients.yaml
echo ".client/client.yaml for cbt config generated."

