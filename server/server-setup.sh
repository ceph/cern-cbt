#!/bin/bash
# A script for setting up cbt ceph cluster nodes. Elements that have to be run ON each node should go in here.

#setup server dependencies
tar -xvf /tmp/dependency.tar -C /tmp --strip-components=2
mv /tmp/config  /root/.ssh/
mv /tmp/profile /etc/
mv /tmp/known_hosts /root/.ssh/
mv /tmp/cbt-configuration.yaml /root/
mv /tmp/pdc-scale-testing /root/
mv /tmp/pdc-scale-testing.pub /root/
cat /root/pdc-scale-testing.pub >> /root/.ssh/authorised_keys # this will balloon, fine... 

# Setup web proxy and ssh
eval "$(ssh-agent -s)"
ssh-add /root/pdc-scale-testing
git config --global http.proxy http://cephtelemetry-proxy.cern.ch:3128
source /etc/profile

# dependencies
dnf update -y
dnf install -y psmisc util-linux coreutils xfsprogs e2fsprogs findutils \
  git wget bzip2 make automake gcc gcc-c++ kernel-devel perf blktrace lsof \
  sysstat screen python3-yaml ipmitool dstat zlib-devel fio iftop iperf3 \
  pdsh pdsh-rcmd-ssh ceph ceph-common chkconfig

# cbt and direct dependencies
cd /root
rm -rf cbt fio pmu-tools FlameGraph
git clone https://github.com/ceph/cbt.git 
cd /root/cbt
git clone https://github.com/andikleen/pmu-tools.git 
git clone https://github.com/brendangregg/FlameGraph.git 

# setup collectl
if [ ! -e "/usr/bin/collectl" ]; then
        mv /etc/init.d /tmp/init.d-old # one time, should fail on subsequent runs
        tar -hxvf /tmp/collectl-4.3.8.src.tar.gz -C /root/
        ./root/collectl/INSTALL
        systemctl daemon-reload
        systemctl start collectl        # start data collection service on host
        systemctl enable collectl       # optional: enable collectl server to be started at boot time
        ls -ltr /var/log/collectl/*     # where output from collectl is kept
fi

# configure other system settings
sed -i 's/Defaults    requiretty/#Defaults    requiretty/g' /etc/sudoers
setenforce 0
( awk '!/SELINUX=/' /etc/selinux/config ; echo "SELINUX=disabled" ) > /tmp/x
mv /tmp/x /etc/selinux/config
systemctl stop nftables && systemctl disable nftables
systemctl stop irqbalance && systemctl disable irqbalance
systemctl restart sshd
