#!/bin/bash

# get available options
tests=$(find /tmp/cbt-test/ . -type f -name "benchmark_config.yaml" | xargs)
opts=()
for i in $tests; do
	runid=$(echo $i | sed 's|.*\(id-[^/]*\).*|\1|')
	runtype=$(cat $i | yq eval '.cluster.benchmark')
	opsize=$(cat $i | yq eval '.cluster.op_size')
	opts+=("$runid $runtype opsize:$opsize")
done

#pick an option
runid=$(printf "%s\n" "${opts[@]}" | fzf --prompt="Select an option: " | cut -d' ' -f1)

if [[ -n $runid ]]; then
	echo "selected: $runid"
else
	echo "didn't pick an option, bailing"
	exit 1
fi

cd "/tmp/cbt-test/results/00000000/$runid/"
pwd
ls
